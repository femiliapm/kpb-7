// merubah / mengganti element
document.getElementById("paragraf1").innerHTML = "Hello World";
const title = document.getElementsByClassName("title");
console.log(title);

// ambil element pertama dari h1
let judul1 = title[0];
judul1.innerHTML = "Latihan JavaScript DOM";

// manipulasi styling
judul1 = document.getElementById("judul1");
judul1.style.color = "blue";
judul1.style.border = "1px solid blue";
judul1.style.padding = "20px";

// memberikan event
const button2 = document.getElementById("btn2");
button2.addEventListener("click", function () {
  alert("Halo selamat pagi");
  const newElement = document.createElement("h2");
  newElement.textContent = "Halo Selamat Pagi";
  const body = document.getElementsByTagName("body");
  body[0].appendChild(newElement);
});

// mereaksikan suatu event
function callUs() {
  alert("Button Hubungi Kami telah diklik!");
}

// mengambil element button buat list
const btnAddList = document.getElementById("btn-create-task");
btnAddList.addEventListener("click", function () {
  const inputAddTask = document.getElementById("add-task-group");
  if (inputAddTask.style.display === "none") {
    inputAddTask.style.display = "block";
    btnAddList.innerText = "Sembunyikan";
  } else {
    inputAddTask.style.display = "none";
    btnAddList.innerText = "Buat List";
  }
});

// Menambahkan list item
// versi 1 ketika ada button
const inputTask = document.getElementById("input-task");
const btnSimpan = document.getElementById("btn-simpan");
btnSimpan.addEventListener("click", function () {
  console.log(inputTask.value);
  const newListItem = document.createElement("li");
  newListItem.textContent = inputTask.value;
  const listGroup = document.getElementById("list-group");
  listGroup.appendChild(newListItem);
  inputTask.value = "";
});

// versi 2 ketika pakai keypress
inputTask.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    const newListItem = document.createElement("li");
    newListItem.textContent = inputTask.value;
    const listGroup = document.getElementById("list-group");
    listGroup.appendChild(newListItem);
    inputTask.value = "";
  }
});

let dataUser;
// fetch data
fetch("https://jsonplaceholder.typicode.com/users")
  .then((response) => response.json())
  .then((data) => {
    dataUser = data;
    console.log(dataUser);

    const groupName = document.getElementById("group-name");
    for (let index = 0; index < dataUser.length; index++) {
      const element = dataUser[index];
      console.log(element);
      const name = document.createElement("li");
      name.textContent = element.name;
      groupName.appendChild(name);
    }
  });
