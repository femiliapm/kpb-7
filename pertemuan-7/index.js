// untuk mengecek bilangan ganjil atau genap
const bilangan = 10;
if (bilangan % 2 == 0) {
  console.log(`${bilangan} merupakan angka genap`);
} else {
  console.log(`${bilangan} merupakan angka ganjil`);
}

let username = "fazz2";
let password = "abcd123";
// logika AND && = akan bernilai true ketika kedua kondisinya terpenuhi
// logika OR || =  akan bernilai true ketika ada salah satu atau kedua kondisinya yg bernilai tru
if (username != "fazz" || password != "abcd1234") {
  console.log("login gagal");
} else {
  console.log("login berhasil");
}

username = "fazz";
password = "abcd1234";

// ternary operator: shortcut if else, hanya punya 2 kondisi yaitu true or false
const isLoggedIn =
  username != "fazz" || password != "abcd1234"
    ? "login gagal"
    : "login berhasil";
console.log(isLoggedIn);

// switch case
let state = 2;
switch (state) {
  case 1:
    console.log("angka satu");
    break;
  case 2:
    console.log("angka dua");
    break;
  default:
    console.log("input angka salah");
    break;
}

// nested if-else
// mengecek apakah bilangan kelipatan 2 atau 3
let angka = 15;
if (angka % 2 == 0) {
  console.log("kelipatan 2");
} else if (angka % 3 == 0) {
  console.log("kelipatan 3");
} else {
  console.log("bukan kelipatan 2 atau 3");
}

// mendapatkan value dari suatu object
const user = {
  username: "user01",
  password: "user0101",
};

// menampilkan username dan password dari object user
console.log(`username: ${user.username}`);
console.log(`password: ${user.password}`);
