// menampilkan bilangan dari 3 - 33
for (let index = 3; index <= 33; index++) {
  if (index % 3 == 0) {
    // console.log(index);
  }
}

let number = 3;
while (number <= 33) {
  if (number % 4 == 0) {
    // console.log(number);
  }
  number++;
}

const arrBuah = ["Rambutan", "pepaya", "jambu", "jeruk"];
// indexing array = [0, 1, 2, 3]
// size / ukuran = 4
// menampilkan buah pepaya dari suatu array
console.log(arrBuah[1]);

for (let index = 0; index < arrBuah.length; index++) {
  if (arrBuah[index] == "Rambutan") {
    console.log(arrBuah[index]);
  }
}

// array of object
const users = [
  {
    username: "user01",
    password: "user0101",
  },
  {
    username: "user02",
    password: "user0202",
  },
  {
    username: "user03",
    password: "user0303",
  },
];

let hasil = users[1];
console.log(hasil.username);

// function
// 1. void function: fungsi yang tidak mengembalikan data / nilai, tanpa return
function perkalianDuaAngka(angka1, angka2) {
  console.log(angka1 * angka2);
}
function greeting() {
  console.log("halo selamat malam!");
}

// 2. return function: fungsi yang mengembalikan data / nilai, return
function penjumlahanDuaAngka(angka1, angka2) {
  return angka1 + angka2;
}

// panggil fungsi
perkalianDuaAngka(2, 9); // 18
let result = penjumlahanDuaAngka(2, 9);
console.log(penjumlahanDuaAngka(2, 9)); // 11
console.log(result);
greeting();

// compare
console.log(true == 1);
console.log(false == "0");
